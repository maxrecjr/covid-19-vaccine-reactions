import pandas as pd
import numpy as np

class Data:

    def _Read_Dataset(self):
        df = pd.read_excel(self.path)
        if (self.columns[0] == "all"):
            print("Reading all columns...")
        else:
            df = df[self.columns]
        return df

    def __init__(self, path, columns, lines):
        print("Inicializing class with paramaters:")
        print("Path: " + str(path))
        print("Columns: " + str(columns))
        print("Lines: " + str(lines))
        self.path = path
        self.columns = columns
        self.lines = lines
        self.dataset = self._Read_Dataset()