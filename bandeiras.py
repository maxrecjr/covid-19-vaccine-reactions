import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
from neuralprophet import NeuralProphet

def make_time_series(df_quer, years, freq, start_inx=0, has_leap_year = None, leap_year = None):
    time_series = []
    # print(df_quer)

    df_quer = np.asarray(df_quer)
    # print(df_quer[0:365])
    for i in range(len(years)):
        year = years[i]
        # print(year)

        if year == '2021':
            end_idx = start_inx+365
            # print(year)

        else:
            end_idx = start_inx+366
            # print(year)


        t_start = year + '-01-01'
        t_end = year + '-12-31'

        data = df_quer[start_inx:end_idx]
        # print(data)
        index = pd.date_range(start=t_start, end=t_end, freq=freq)
        # print(index)
        time_series.append(pd.Series(data=data, index=index, dtype=int))
        # print(time_series)

        start_inx = end_idx

    return time_series

def create_training_series(complete, predic_len):
    time_series_training = []
    for ts in complete:
        time_series_training.append(ts[: -predic_len])

    return time_series_training

df = pd.read_csv('./dataset/casos_pr/casos.csv')

subset = df[['date','state','totalCases','deaths','newCases','newDeaths','recovered','suspects','tests','vaccinated']]
subset = subset[subset['state'] == 'RJ']
datas = pd.to_datetime(subset['date'])


plt.figure()
plt.title("Número de casos por dia em 2020 e 2021")
plt.xlabel("Data")
plt.ylabel("Número de casos")
plt.plot(datas, subset['newCases'])
plt.plot(datas, subset['newDeaths'])
plt.legend(["Novos casos","Novos óbitos"])
plt.grid()
# plt.show()


series = pd.DataFrame({'ds':subset['date'],'y':subset['newCases']})
series = series.reset_index()
novo = series[:600]
novo = novo.drop('index', axis='columns')

resto = series[600:645]
resto = resto.drop('index', axis='columns')
print(novo, resto)


frq = 'D'
Rede = NeuralProphet()
Rede.fit(novo, frq, epochs=1000)

future = Rede.make_future_dataframe(novo, periods=45)
forecast = Rede.predict(future)
print(forecast.head())
print(forecast.tail())
print(resto)
datas_prev = pd.to_datetime(forecast['ds'])
datas_real = pd.to_datetime(resto['ds'])

plt.figure()
plt.title('Plot da previsibilidade de novos casos no final de 2020')
plt.plot(datas_prev, forecast['yhat1'])
plt.plot(datas_real, resto['y'])
plt.legend(["Previsao de casos","Casos confirmados"])
plt.grid()
plt.show()
# plot = Rede.plot(forecast)
# print(type(forecast['ds']))