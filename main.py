import numpy as np
import pandas as pd
from src.data import Data
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

data_person = Data("./dataset/2021VAERSDATA.xlsx", ["VAERS_ID", "DIED", "AGE_YRS"], (0,100))
data_sintomas = Data("./dataset/2021VAERSSYMPTOMS.xlsx", ["VAERS_ID", "SYMPTOM1","SYMPTOM2","SYMPTOM3"], (0,100))
data_vax = Data("./dataset/2021VAERSVAX.xlsx", ["VAERS_ID","VAX_MANU"], (0,100))
#print(data_person.dataset)
#print(data_sintomas.dataset)
# print(data_vax.dataset)

data_vax.dataset = data_vax.dataset[data_vax.dataset["VAX_MANU"].isin(["MODERNA","PFIZER\BIONTECH"])]
data_person.dataset = data_person.dataset[data_person.dataset["VAERS_ID"].isin(data_vax.dataset["VAERS_ID"])]
data_sintomas.dataset = data_sintomas.dataset[data_sintomas.dataset["VAERS_ID"].isin(data_vax.dataset["VAERS_ID"])]

data_vax.dataset.drop_duplicates(subset = "VAERS_ID")
data_person.dataset.drop_duplicates(subset = "VAERS_ID")
data_sintomas.dataset.drop_duplicates(subset = "VAERS_ID")
cont = 0
# data_sintomas.dataset["VAX_MANU"] = np.nan
# print(len(data_sintomas.dataset))

data_sintomas.dataset = pd.merge(data_sintomas.dataset, data_vax.dataset, left_on="VAERS_ID", right_on="VAERS_ID")
data_sintomas.dataset = pd.merge(data_sintomas.dataset, data_person.dataset, left_on="VAERS_ID", right_on="VAERS_ID")

data_sintomas.dataset["VAX_MANU"] = data_sintomas.dataset["VAX_MANU"].replace("MODERNA", 0)
data_sintomas.dataset["VAX_MANU"] = data_sintomas.dataset["VAX_MANU"].replace("PFIZER\BIONTECH", 1)
data_sintomas.dataset = data_sintomas.dataset.dropna(subset=["AGE_YRS"])
data_sintomas.dataset["DIED"] = data_sintomas.dataset["DIED"].replace(np.nan, 0)
data_sintomas.dataset["DIED"] = data_sintomas.dataset["DIED"].replace("Y", 1)

df = pd.DataFrame(data_sintomas.dataset["SYMPTOM1"])
df2 = pd.DataFrame({"SYMPTOM1":data_sintomas.dataset["SYMPTOM2"]})
df2 = df2.dropna()
df3 = pd.DataFrame({"SYMPTOM1":data_sintomas.dataset["SYMPTOM3"]})
df3 = df3.dropna()

df = df.append(df2)
df = df.append(df3)

df = df.drop_duplicates(subset = "SYMPTOM1")

array = np.asarray(df["SYMPTOM1"])

for i in range(0, len(array)):
    data_sintomas.dataset["SYMPTOM1"] = data_sintomas.dataset["SYMPTOM1"].replace(array[i], i)
    data_sintomas.dataset["SYMPTOM2"] = data_sintomas.dataset["SYMPTOM2"].replace(array[i], i)
    data_sintomas.dataset["SYMPTOM3"] = data_sintomas.dataset["SYMPTOM3"].replace(array[i], i)

data_sintomas.dataset["SYMPTOM1"] = data_sintomas.dataset["SYMPTOM1"].replace(np.nan, -1)
data_sintomas.dataset["SYMPTOM2"] = data_sintomas.dataset["SYMPTOM2"].replace(np.nan, -1)
data_sintomas.dataset["SYMPTOM3"] = data_sintomas.dataset["SYMPTOM3"].replace(np.nan, -1)

data_cluster = pd.DataFrame({"S1":data_sintomas.dataset["SYMPTOM1"],"S2":data_sintomas.dataset["SYMPTOM2"],"S3":data_sintomas.dataset["SYMPTOM3"],"Y":data_sintomas.dataset["AGE_YRS"],"V":data_sintomas.dataset["VAX_MANU"]})

clusters = 2
kmeans = KMeans(n_clusters = clusters)
dfcluster = kmeans.fit(data_cluster)
array = np.asarray(data_sintomas.dataset["DIED"])

cont = 0
colors = ['#DF2020', '#81DF20']
colors_cluster = []
colors_real = []
for i in range(len(array)):
    if(kmeans.labels_[i] == array[i]):
        cont = cont + 1
    if(array[i] == 0):
        colors_real.append(colors[0])
    else:
        colors_real.append(colors[1])
    if(kmeans.labels_[i] == 0):
        colors_cluster.append(colors[0])
    else:
        colors_cluster.append(colors[1])

colors_real = np.asarray(colors_real)
colors_cluster = np.asarray(colors_cluster)

data_cluster["C_D"] = kmeans.labels_
data_cluster["C_C"] = colors_cluster
data_cluster["R_C"] = colors_real
data_cluster['D'] = array

print(data_sintomas.dataset)
print(data_cluster)
wr = 100*cont/len(array)
print("TAXA DE ACERTO: " + str(wr))

plt.figure()
plt.title("CLUSTER")
plt.xlabel("Sintomas")
plt.ylabel("Idade")
plt.scatter(data_cluster['S1'], data_cluster['Y'], c=data_cluster['C_C'])
plt.figure()
plt.title("REAL")
plt.xlabel("Sintomas")
plt.ylabel("Idade")
plt.scatter(data_cluster['S1'], data_cluster['Y'], c=data_cluster['R_C'])
plt.show()